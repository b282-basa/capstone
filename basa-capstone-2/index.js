const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const bodyParser = require('body-parser');

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");

const app = express();

//mongoose.connect("mongodb+srv://marixbasa:cute@wdc028-course-booking.aun96z2.mongodb.net/ECommerceAPI",

//mongoose.connect("mongodb+srv://marixbasa:cute@wdc028-course-booking.aun96z2.mongodb.net/Capstone2",

mongoose.connect("mongodb+srv://marixbasa:cute@wdc028-course-booking.aun96z2.mongodb.net/CapstoneProject2",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get("/"), (req, res) =>
{
	res.render("/index.html")
}
app.use(cors());

app.use(bodyParser.json());

app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute);

app.listen(process.env.PORT || 4001, () => console.log(`Now listening to port ${process.env.PORT || 4001}!`));