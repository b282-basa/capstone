const mongoose = require("mongoose");

// OPTION 1
/*const userSchema = new mongoose.Schema(
{
	email: 
	{
		type: String,
		required: [true, "Email is required!"]	
	},

	password:
	{
		type: String,
		required: [true, "Password is required!"]
	},

	isAdmin:
	{
		type: Boolean,
		default: false
	},

	orderedProduct:
	[
		{
			products: 
			[
				{	
						productId:
						{
							type: String,
							required: [true, "Product ID is required!"]
						},

						productName:
						{
							type: String,
							required: [true, "Product ID is required!"]
						},

						quantity:
						{
							type: Number,
							required: [true, "Quantity is required!"]
						},
				}
			],

			totalAmount:
				{
					type: Number,
					required: [true, "Total Amount is required!"]
				}, 

			purchasedOn: 
				{
					type: Date,
					default: new Date()
				},

		}
	]

});*/


// OPTION 2
/*const userSchema = new mongoose.Schema(
{
	email: 
	{
		type: String,
		required: [true, "Email is required!"]	
	},

	password:
	{
		type: String,
		required: [true, "Password is required!"]
	},

	isAdmin:
	{
		type: Boolean,
		default: false
	},
});*/


// OPTION 2 Version 2
const userSchema = new mongoose.Schema(
{
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},

	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},

	email: 
	{
		type: String,
		required: [true, "Email is required!"]	
	},

	password:
	{
		type: String,
		required: [true, "Password is required!"]
	},


	isAdmin:
	{
		type: Boolean,
		default: false
	},
});
module.exports = mongoose.model("User", userSchema);