const mongoose = require("mongoose");

// OPTION 1
/*const productSchema = new mongoose.Schema(
{
	name: 
	{
		type: String,
		required: [true, "Product name is required!"]	
	},

	description: 
	{
		type: String,
		required: [true, "Description is required!"]	
	},

	price: 
	{
		type: Number,
		required: [true, "Price is required!"]	
	},

	isActive:
	{
		type: Boolean,
		default: true
	},

	createdOn: 
	{
		type: Date,
		default: new Date()
	},

	userOrders: 
	[
	{
		userId: 
		{
			type: String,
			required: [true, "User ID is required!"]		
		},

		orderId: 
		{
			type: String,
			required: [true, "Order ID is required!"]		
		}
	}
	]
});*/



//OPTION 2
const productSchema = new mongoose.Schema(
{
	name: 
	{
		type: String,
		required: [true, "Product name is required!"]	
	},

	description: 
	{
		type: String,
		required: [true, "Description is required!"]	
	},

	price: 
	{
		type: Number,
		required: [true, "Price is required!"]	
	},

	isActive:
	{
		type: Boolean,
		default: true,
	},

	createdOn: 
	{
		type: Date,
		default: new Date()
	},
});

module.exports = mongoose.model("Product", productSchema);