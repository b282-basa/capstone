const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
{
	userId: 
	{
		type: String,
		required: [true, "User ID is required!"]	
	},

	email:
	{
		type: String,
		required: [true, "Email is required!"]	
	},

	products: 
	[
	{
		productId: 
		{
			type: String,
			required: [true, "Product ID is required!"]		
		},

		productName:
		{
			type: String,
			required: [true, "Product Name is required!"]		
		},

		price: 
		{
			type: Number,
			required: [true, "Price is required!"]	
		},

		quantity:
		{
			type: Number,
			required: [true, "Quantity is required!"],
			default: 0
		},

		subtotal:
		{
			type: Number,
			required: [true, "Subtotal is required!"],
			default: 0
		},


	}
	],

	totalAmount:
	{
		type: Number,
        required: [true, "Total Amount is required"],
        default: 0

	},

	purchasedOn: 
	{
		type: Date,
		default: new Date()
	},
});




module.exports = mongoose.model("Order", orderSchema);