const express = require("express");

const router = express.Router();

const userController = require("../controllers/userController");
const productController = require("../controllers/productController");
const orderController = require("../controllers/orderController");

const auth = require("../auth");


// Route for user registration

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for user authentication (login)

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving user details

// using link version
/*router.get("/:userId", (req, res) => {

	userController.retrieveUserDetails(req.params).then(resultFromController => res.send(resultFromController));
});*/

//using auth version
router.get("/details", auth.verify, (req, res) => {
	const user = {
		userId: auth.decode(req.headers.authorization).id
	}

	userController.retrieveUserDetails(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
});


// Route to set user as admin (ADMIN)

// body version
/*router.put("/updateadminstatus", auth.verify, (req, res) => {
	const admin = 
	{
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	userController.updateAdminStatus(admin, req.params, req.body).then(resultFromController => res.send(resultFromController));
});*/

// endpoint version
router.put("/updateadminstatus/:userId", auth.verify, (req, res) => {
	const admin = 
	{
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	userController.updateAdminStatus(admin, req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Routes for checking if the user's email already exist in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

module.exports = router;