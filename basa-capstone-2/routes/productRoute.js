const express = require("express");

const router = express.Router();

const userController = require("../controllers/userController");
const productController = require("../controllers/productController");
const orderController = require("../controllers/orderController");

const auth = require("../auth");


// Router for creating Product (ADMIN)

router.post("/createproduct", auth.verify, (req, res) => {

	const data = 
	{
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.createProduct(data).then(resultFromController => res.send(resultFromController));
});


// Router for retrieving all products (ADMIN)

// admin access
// capstone 2
/*router.get("/allproduct", auth.verify, (req,res) => {
	
	const data = 
	{
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	productController.retrieveAllProduct(data).then(resultFromController => res.send(resultFromController));
});*/

// capstone 3
router.get("/allproduct", (req,res) => {
	
	productController.retrieveAllProduct().then(resultFromController => res.send(resultFromController));
});

// Router for retrieving all active products

router.get("/activeproduct", (req,res) => {
	productController.retrieveAllActiveProduct().then(resultFromController => res.send(resultFromController));
});

// Router for retrieving all inactive products

router.get("/inactiveproduct", (req,res) => {
	productController.retrieveAllInActiveProduct().then(resultFromController => res.send(resultFromController));
});



// Router for retrieving single product

//sa link version
//capstone 2
/*router.get("/singleproduct/:productId", (req, res) => {
	productController.retrieveSingleProduct(req.params).then(resultFromController => res.send(resultFromController));
});*/

// capstone 3

router.get("/singleproduct/:productId", (req, res) => {
	productController.retrieveSingleProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// sa body version
/*router.get("/singleproduct", auth.verify, (req, res) => {
	const data = 
	{
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	productController.retrieveSingleProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});*/

/*router.get("/singleproduct" (req, res) => {
	const admin = 
	{
		isAdmin: isAdmin 
	};

	productController.retrieveSingleProduct(req.body, admin).then(resultFromController => res.send(resultFromController));
})*/

router.post("/singleproduct", (req, res) => {
	const admin = 
	{
		isAdmin: isAdmin 
	};
	productController.retrieveSingleProduct(req.body, admin).then(resultFromController => res.send(resultFromController));
});



// Route for updating product information (ADMIN)


router.put("/update/:productId", auth.verify, (req, res) => {
	const admin = 
	{
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		product: req.body
	};
	productController.updateProductInformation(admin, req.params, req.body).then(resultFromController => res.send(resultFromController));
});


// Router to archive product (ADMIN)

router.patch("/archive/:productId", auth.verify, (req, res) => {
	const admin = 
	{
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	productController.archiveProduct(admin, req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Router to activate product (ADMIN)

router.patch("/activate/:productId", auth.verify, (req, res) => {
	const admin = 
	{
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	productController.activateProduct(admin, req.params, req.body).then(resultFromController => res.send(resultFromController));
});




router.delete("/deleteproduct/:productId", auth.verify, (req, res) => {
	const admin = 
	{
		isAdmin: auth.decode(req.headers.authorization).isAdmin,

	};
	productController.deleteProduct(admin, req.params, req.body).then(resultFromController => res.send(resultFromController));
});


module.exports = router;