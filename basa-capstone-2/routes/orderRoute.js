const express = require("express");

const router = express.Router();

const userController = require("../controllers/userController");
const productController = require("../controllers/productController");
const orderController = require("../controllers/orderController");

const auth = require("../auth");


//create order (non-admin)

// BODY VERSION
/*router.patch('/createorder', auth.verify, (req, res) => {
    let data = {

    	isAdmin: auth.decode(req.headers.authorization).isAdmin,
        userId: auth.decode(req.headers.authorization).id,
        //userId: req.body.userId,
        productId: req.body.productId,
        quantity: req.body.quantity
    }

    orderController.createOrder(data, req.params, req.body).then(resultFromController => res.send(resultFromController))
});
*/


// URL VERSION
router.patch('/createorder/:productId', auth.verify, (req, res) => {
    let data = {

    	isAdmin: auth.decode(req.headers.authorization).isAdmin,
        userId: auth.decode(req.headers.authorization).id,
        //userId: req.body.userId,
        productId: req.params.productId,
        quantity: req.body.quantity
    }

    orderController.createOrder(data, req.params.id, req.body).then(resultFromController => res.send(resultFromController))
});



// ROUTE: retrieve all order (ADMIN)

router.get("/allorder", auth.verify, (req,res) => {
	
	const data = 
	{
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	orderController.retrieveAllOrder(data).then(resultFromController => res.send(resultFromController));
});



// ROUTE: retrieve authenticated user's orders

/*router.get("/userorder", auth.verify, (req,res) => {
	
	const data = 
	{
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id,
	};
	orderController.retrieveUserOrder(data).then(resultFromController => res.send(resultFromController));
});*/

router.get("/userorder/:userId", auth.verify, (req,res) => {
	
	const data = 
	{
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id,
	};
	orderController.retrieveUserOrder(data, req.Params).then(resultFromController => res.send(resultFromController));
});



module.exports = router;