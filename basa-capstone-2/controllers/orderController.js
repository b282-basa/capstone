const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

const bcrypt = require("bcrypt");

const auth = require("../auth");



// CONTROLLER : non admin create order
// BODY VERSION
/*module.exports.createOrder = async (data, reqParams, reqBody) => {
	// non-admin
	if (!data.isAdmin)
	{
		// with existing orders inside the user id
		//let checkExistingOrder = await Order.find({userId: data.userId}).then(result =>
		let checkExistingOrder = await Order.find({userId: data.userId}).then(result =>
		{
			if (result.length > 0) 
			{
				return true;
			}
			else
			{
				return false;
			}
		});

		if (checkExistingOrder)
		{
			let price = await Product.findById(data.productId).then(result => {
				return result.price;
			});

			let quantity = await Order.find({productId: data.productId}).then(result => {
				return data.quantity;
			});


			let subtotal = await Order.find({productId: data.productId}).then( result => {
				let st = price * quantity;
				return st;
			});


			let newOrder = {
				productId: data.productId,
				quantity: quantity,
				subtotal: subtotal
			}

			let updateOrder = await Order.findOne({userId: data.userId}).then(result => {
				result.products.push(newOrder);
				result.totalAmount = result.totalAmount + subtotal;
				result.purchasedOn = new Date();

				return result.save().then((order, error) => {
					if (error)
					{
						//return false;
						return {message: "Error Encountered"};
					}
					else
					{
						//return true;
						return {message: "Ordered Product was succesfully saved."};
					}
				});
			});

			if (checkExistingOrder && updateOrder) 
			{
				//return true;
				return {message: "Ordered Product was succesfully added together with existing orders."};
			}
			else
			{
				//return false;
				return {message: "Unable to add order."};

			}
		}

		else 
		{

			// no orders yet
			let price = await Product.findById(data.productId).then(result => {
				return result.price;
			});

			let quantity = await Order.find({productId: data.productId}).then(result => {
				return data.quantity;
			});


			let subtotal = await Order.find({productId: data.productId}).then( result => {
				let st = price * quantity;
				return st;
			});


			let newOrder = new Order({
				userId: data.userId,
				products: {
					productId: data.productId,
					quantity: quantity,
					subtotal: subtotal
				},
				totalAmount: subtotal,
				purchasedOn: new Date()
			});

			return newOrder.save().then((order, error) =>{
            
            if(error) 
            {
               //return false;
            	return {message: "Error Encountered"};
            } 
            else 
            {
           		//return true;
           		return {message: "Ordered Product was succesfully added in new order. "};
            }
            });

		}

	}

	if (data.isAdmin)
	{	// if admin
		let message = Promise.resolve("Only non-admin can create Order")

   	 		return message.then((value) => {
       			return {value}
				});
   	}

};*/


// URL VERSION
module.exports.createOrder = async (data, reqParams, reqBody) => {
	// non-admin
	if (!data.isAdmin)
	{
		// with existing orders inside the user id
		//let checkExistingOrder = await Order.find({userId: data.userId}).then(result =>
		let checkExistingOrder = await Order.find({userId: data.userId}).then(result =>
		{
			if (result.length > 0) 
			{
				return true;
			}
			else
			{
				return false;
			}
		});

		if (checkExistingOrder)
		{

			let email = await User.findById(data.userId).then(result => {
				return result.email;
			});

			let productName = await Product.findById(data.productId).then(result => {
				return result.name;
			});

			let price = await Product.findById(data.productId).then(result => {
				return result.price;
			});

			let quantity = await Order.findOne({productId: data.productId}).then(result => {
				return data.quantity;
			});


			let subtotal = await Order.findOne({productId: data.productId}).then( result => {
				let st = price * quantity;
				return st;
			});


			let newOrder = {
				userId: data.userId,
				email: email,
				productId: data.productId,
				productName: productName,
				price: price,
				quantity: quantity,
				subtotal: subtotal
			}

			let updateOrder = await Order.findOne({userId: data.userId}).then(result => {
				result.products.push(newOrder);
				result.totalAmount = result.totalAmount + subtotal;
				result.purchasedOn = new Date();

				return result.save().then((order, error) => {
					if (error)
					{
						//return false;
						return {message: "Error Encountered"};
					}
					else
					{
						//return true;
						return {message: "Ordered Product was succesfully saved."};
					}
				});
			});

			if (checkExistingOrder && updateOrder) 
			{
				//return true;
				return {message: "Ordered Product was succesfully added together with existing orders."};
			}
			else
			{
				//return false;
				return {message: "Unable to add order."};

			}
		}

		else 
		{

			let email = await User.findById(data.userId).then(result => {
				return result.email;
			});

			let productName = await Product.findById(data.productId).then(result => {
				return result.name;
			});


			// no orders yet
			let price = await Product.findById(data.productId).then(result => {
				return result.price;
			});

			let quantity = await Order.findOne({productId: data.productId}).then(result => {
				return data.quantity;
			});


			let subtotal = await Order.findOne({productId: data.productId}).then( result => {
				let st = price * quantity;
				return st;
			});


			let newOrder = new Order({
				userId: data.userId,
				email: email,
				products: {
					productId: data.productId,
					productName: productName,
					price: price,
					quantity: quantity,
					subtotal: subtotal
				},
				totalAmount: subtotal,
				purchasedOn: new Date()
			});

			return newOrder.save().then((order, error) =>{
            
            if(error) 
            {
               //return false;
            	return {message: "Error Encountered"};
            } 
            else 
            {
           		//return true;
           		return {message: "Ordered Product was succesfully added in new order. "};
            }
            });

		}

	}

	if (data.isAdmin)
	{	// if admin
		let message = Promise.resolve("Only non-admin can create Order")

   	 		return message.then((value) => {
       			return {value}
				});
   	}

};

// CONTROLLER: retrieve all orders (ADMIN)


module.exports.retrieveAllOrder = (data) => {
	if (data.isAdmin) 
	{
		return Order.find({}).then((result, error) => {
			if (error) 
			{
				//return false;
				return {message: "Error Encountered."};
			} 
			else 
			{
				return result;
			};
		});
	}	
	else
	{
		let message = Promise.resolve("User must be Admin to access this.")
		return message.then((value) => {
			return {value}
		});
	};
};


// CONTROLLER: retrieve authenticated user's orders 


// ETO UNG NASA HISTORY 
/*module.exports.retrieveUserOrder = (data) => {
	if (!data.isAdmin) 
	{
		return Order.find({userId: data.userId}).then((result, error) => {
			if (error) 
			{
				//return false;
				return {message: "Error Encountered."};
			} 
			else 
			{
				return result;
			};
		});
	}	
	else
	{
		let message = Promise.resolve("User must not be an Admin to access this.")
		return message.then((value) => {
			return {value}
		});
	};
};*/

//capstone 3
module.exports.retrieveUserOrder = (data, reqParams) => {
	if (!data.isAdmin) 
	{
		return Order.findById(reqParams.userId).then((result, error) => {
			if (error) 
			{
				//return false;
				return {message: "Error Encountered."};
			} 
			else 
			{
				return result;
			};
		});
	}	
	else
	{
		let message = Promise.resolve("User must not be an Admin to access this.")
		return message.then((value) => {
			return {value}
		});
	};
};