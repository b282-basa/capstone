const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

const bcrypt = require("bcrypt");

const auth = require("../auth");

// Controller for Creating Product (ADMIN)


// OPTION 1
/*module.exports.createProduct = (data) => {
	if (data.isAdmin) 
	{

		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});

		return newProduct.save().then((product, error) => {

			if (error) 
			{
				//return false;
				return {message: "Error Encountered."};
			} 
			else 
			{
				//return true;
				return {message: "New Product was succesfully created."};
			};
		});

	};

	let message = Promise.resolve("User must be Admin to access this.")
	return message.then((value) => {
		return {value}
	});
};*/

// OPTION 2

module.exports.createProduct = (data) => {
	if (data.isAdmin) 
	{

		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});

		return newProduct.save().then((product, error) => {

			if (error) {
				//return false;
				return {message: "Error Encountered."};
			} else {
				//return true;
				return {message: "New Product was succesfully created."};
			};
		});

	};

	let message = Promise.resolve("User must be Admin to access this.")
	return message.then((value) => {
		return {value}
	});
};


/*// Controller for retrieving all products
module.exports.retrieveAllProduct= (data) => {
	if (data.isAdmin) 
	{
	return Product.find({}).then((result, error) => {
		return result; 
		if (error) {
				return false;
			} else {
				return true;
			};
	});
}

else
{
	let message = Promise.resolve("User must be Admin to access this.")
	return message.then((value) => {
		return {value}
	});
}
};*/

// Controller for retrieving all products ()
/*module.exports.retrieveAllProduct = () => {
	return Product.find({}).then(result => {
		return result; 
	});
};*/

// capstone 2
/*module.exports.retrieveAllProduct = (data) => {
	if (data.isAdmin) 
	{
		return Product.find({}).then((result, error) => {
			if (error) 
			{
				//return false;
				return {message: "Error Encountered."};
			} 
			else 
			{
				return result;
			};
		});
	}	
	else
	{
		let message = Promise.resolve("User must be Admin to access this.")
		return message.then((value) => {
			//return {value}

			return {message: "Back-end (Retrieve All Product)"}
		});
	};
};*/

// capstone 3

module.exports.retrieveAllProduct = () => {
	return Product.find({}).then(result => {
		return result; 
	});
};


// Controller for retrieving all active products

module.exports.retrieveAllActiveProduct = () => {
	return Product.find({isActive: true}).then(result => {
		return result; 
	});
};

// Controller for retrieving all inactive products

module.exports.retrieveAllInActiveProduct = () => {
	return Product.find({isActive: false}).then(result => {
		return result; 
	});
};


// Controller for retrieving single product

// sa link version
/*module.exports.retrieveSingleProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};*/

// capstone 2
/*module.exports.retrieveSingleProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};*/

// capstone 2
module.exports.retrieveSingleProduct = (reqBody, admin) => {

	if (admin.isAdmin)
		console.log("unauthorize")
	else
		{
		return Product.find({name: reqBody.name, isActive: true}).then(result => {
			return result;
		});
	}
};

//capstone 3
module.exports.retrieveSingleProduct = (reqParams) => {
		return Product.findById(reqParams.productId).then(result => {
			return result;
		});
	}

// sa body version
/*module.exports.retrieveSingleProduct = (reqBody) => {
	return Product.findById(reqBody.productId).then(result => {
		return result;
	});
};*/

// Controller for updating product information (ADMIN)


module.exports.updateProductInformation = (admin, reqParams, reqBody) => {
	if (admin.isAdmin) 
	{
		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			isActive: reqBody.isActive
		};

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		//return Product.findByIdAndUpdate(reqBody.productId, updatedProduct).then((product, error) => {
			/*if (error) {
				//return false;
				return {message: "Error Encountered."};
			} else {
				//return true;
				return {message: "Product was modified with changes."};
			};*/

			if (product) {
				//return false;

				return {message: "Product was modified with changes."};
				
			} else {
				//return true;
				return {message: "Error Encountered."};
			};

		});
	}
	else
	{
		let message = Promise.resolve("User must be Admin to access this.")
		return message.then((value) => {
			return {value}
		});
	};
};

// Controller to archive product (ADMIN)

module.exports.archiveProduct = (admin, reqParams, reqBody) => {
	if (admin.isAdmin) 
	{
		let updatedActiveProduct = 
		{
			isActive: false
		};

		return Product.findByIdAndUpdate(reqParams.productId, updatedActiveProduct).then((product, error) => {
			if (error) {
				//return false;
				return {message: "Error Encountered."};
			} else {
				//return true;
				return {message: "Product was placed into archived status."};
			};
		});
	}
	else
	{
		let message = Promise.resolve("User must be Admin to access this.")
		return message.then((value) => {
			return {value}
		});
	};
};

// Controller to activate product (ADMIN)

module.exports.activateProduct = (admin, reqParams, reqBody) => {
	if (admin.isAdmin) 
	{
		let updatedActiveProduct = 
		{
			isActive: true
		};

		return Product.findByIdAndUpdate(reqParams.productId, updatedActiveProduct).then((product, error) => {
			if (error) {
				//return false;
				return {message: "Error Encountered."};
			} else {
				//return true;
				return {message: "Product was placed into actived status."};
			};
		});
	}
	else
	{
		let message = Promise.resolve("User must be Admin to access this.")
		return message.then((value) => {
			return {value}
		});
	};
};

// delete
module.exports.deleteProduct = (admin, reqParams, reqBody) => {
	if (admin.isAdmin) 
	{

		return Product.findByIdAndDelete(reqParams.productId).then((product, error) => {
			if (error) {
				//return false;
				return {message: "Error Encountered."};
			} else {
				//return true;
				return {message: "Product was placed into deleted status."};
			};
		});
	}
	else
	{
		let message = Promise.resolve("User must be Admin to access this.")
		return message.then((value) => {
			return {value}
		});
	};
};

