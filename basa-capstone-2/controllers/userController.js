const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

const bcrypt = require("bcrypt");

const auth = require("../auth");


// User Registration

// DURING CAPSTONE 2
module.exports.registerUser = (reqBody) => {

	let newUser = new User(
	{
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email : reqBody.email,
		mobileNo: reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)

	});
	return newUser.save().then((user, error) => {

/*		if (error) {
			//return false;

			return {message: "Error Encountered."};

		} 

		else {
			//return true; //accepted

			return {message: "New User was succesfully created."};
		};*/

		if (user)
		{
			return {message: "Back-end: New User was succesfully created."}
		}
		else if (error)
		{
			return {message: "Back-end: Error Encountered."};
		}
	});
};

// DURING CAPSTONE 3

// DOCU: https://stackoverflow.com/questions/57547133/find-if-username-and-email-exist-in-mongodb


module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the Postman via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if (result.length > 0) {
			return true;
		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false;
		};
	});
};


/*module.exports.registerUser = async (reqBody, res) => {

	const {email, password, cpassword} = reqBody;

	if (!email || !password || !cpassword)
	{
		return {message: "Inc"};
	}

	try
	{

		const userExist = await User.findOne({email: email});

		if (userExist)
		{
			return {message: "email exist"};
		}

		else if (password !== cpassword)
		{
			return {message: "password not match"};
		}

		else
		{
			const user = new User({email, password, cpassword});

			await user.save();
			return {message: "user registered successfully"};
		}
	}
	catch (err)
	{
		console.log(err)
	}
}*/


// User authentication

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			//return false;
			//return {message: "Inputted credentials are not found in the database."};
			console.log("Inputted credentials are not found in the database.");
			return {message: "Back-end: Inputted credentials are not found in the database."};
		} 
		else 
		{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect)
			{
				return { access : auth.createAccessToken(result) }

			} 

			
			else 
			{
				//return false;
				//return false; //("Incorrect credentials");
				return {message: "User successfully login."};

			};
		};
	});
};


// Controller to retrieve user details

// using link version
/*module.exports.retrieveUserDetails = (reqParams) => {
return User.findById(reqParams.userId).then(result => {
	result.password = "";
	return result;
	});
};
*/
// using auth version

module.exports.retrieveUserDetails = (reqParams) => {
return User.findById(reqParams.userId).then(result => {
	result.password = "";
	return result;
	});
};


// Controller to set the user as admin (ADMIN)

// as body version
/*module.exports.updateAdminStatus = (admin, reqParams, reqBody) => {
	if (admin.isAdmin) 
	{
		let updatedAdminStatus= {
			isAdmin: true
		};

		return User.findByIdAndUpdate(reqBody.userId, updatedAdminStatus).then((product, error) => {
			if (error) {
				//return false;
				return {message: "Error Encountered."};
			} else {
				//return true;
				return {message: "User has granted an admin priviledges."};
			};
		});
	}
	else
	{
		let message = Promise.resolve("User must be Admin to access this.")
		return message.then((value) => {
			return {value}
		});
	};
};*/

// as endpoint version

module.exports.updateAdminStatus = (admin, reqParams, reqBody) => {
	if (admin.isAdmin) 
	{
		let updatedAdminStatus= {
			isAdmin: true
		};

		return User.findByIdAndUpdate(reqParams.userId, updatedAdminStatus).then((product, error) => {
			if (error) {
				//return false;
				return {message: "Error Encountered."};
			} else {
				//return true;
				return {message: "User has granted an admin priviledges."};
			};
		});
	}
	else
	{
		let message = Promise.resolve("User must be Admin to access this.")
		return message.then((value) => {
			return {value}
		});
	};
};