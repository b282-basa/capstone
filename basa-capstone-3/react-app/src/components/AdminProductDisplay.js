//import {useState, useEffect, useContext} from 'react';
import {useState, useEffect} from 'react';

//import { Button, Row, Col, Card, Table, Form, Container} from 'react-bootstrap';
import { Button, Table, Form, Container} from 'react-bootstrap';
import Swal from 'sweetalert2';

//import {Link} from 'react-router-dom';
//import UserContext from '../UserContext';

//export default function ProductDisplay({products}) {
export default function AdminProductDisplay() {

//const {name, description, price, isActive, _id} = products;

//const {user} = useContext(UserContext);

  const [products, setProducts] = useState([]);
  const [newProduct, setNewProduct] = useState({ name: '', description: '', price: 0, isActive: true });
  const [editProduct, setEditProduct] = useState(null);


  function getAllProducts () 
  {
    fetch(`${process.env.REACT_APP_API_URL}/products/allproduct`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}` 
      }
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      })
      .catch((error) => {
        console.error('Error fetching products:', error);
      });
  };

  useEffect(() => {
    getAllProducts();
  }, []);



  const forSubmit = (e) => {
    e.preventDefault();

    const updatedProduct = { ...newProduct };

    if (editProduct) {
      fetch(`${process.env.REACT_APP_API_URL}/products/update/${editProduct._id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(updatedProduct)
      })
      .then((res) => res.json())
      .then((data) => {
        if(data) {
          console.log('Update Product:', updatedProduct);

          Swal.fire({
            title: `Successfully updated`,
            icon: "success",
            text: "Product Updated."
          });
          getAllProducts();
        } else {
          console.log('Failed:', data.error);
        }
      })
      .catch((error) => {
        console.error('Error:', error);
      })

    } 

    else {

      fetch(`${process.env.REACT_APP_API_URL}/products/createproduct`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(newProduct)
      })
      .then((res) => res.json())
      .then((data) => {

        if(data) {
          console.log('New Product:', newProduct);

          Swal.fire({
            title: `Successfully added`,
            icon: "success",
            text: "Product Added."
          });

          getAllProducts();
        } else {
          console.error('Failed:', data.error);
        }
      })
      .catch((error) => {
        console.error('Error Add Product:', error);
      })
      
      console.log('New product:', newProduct);
    }


    setNewProduct({ name: '', description: '', price: 0, isActive: true });
    setEditProduct(null);
    getAllProducts();
  };


  const forAchivingActivingProduct = (productId, isActive) => {
  
  const url = isActive
    ? 

    `${process.env.REACT_APP_API_URL}/products/archive/${productId}`
    
    : 

    `${process.env.REACT_APP_API_URL}/products/activate/${productId}`;

    fetch(url, 
    {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
    })
    .then((res) => res.json())
    .then((data) => {
      
      console.log(data);

      getAllProducts();

    })
    .catch((error) => {
      console.error('Error:', error);
    });
  };

  const forUpdateProduct = (product) => {
    setEditProduct(product);
    setNewProduct({ ...product });
  };

  const forDeleteProduct = (productId) => {

    fetch(`${process.env.REACT_APP_API_URL}/products/deleteproduct/${productId}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}` 
      }
    })
    .then((res) => res.json())
    .then((data) => {
      
      console.log('Delete product:', productId);

      getAllProducts();
    })
    .catch((error) => {
      console.error('Error deleting product:', error);
    });

  };


return (
   
    <Container>
      <div>
      <h2 className="title"> Product Information</h2>
      
      <Form onSubmit={forSubmit}>
        <Form.Group controlId="productName">
          <Form.Label>Product Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter product name"
            value={newProduct.name}
            onChange={(e) => setNewProduct({ ...newProduct, name: e.target.value })}
            required
          />
        </Form.Group>

        <Form.Group controlId="productDescription">
          <Form.Label>Description</Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            placeholder="Enter product description"
            value={newProduct.description}
            onChange={(e) => setNewProduct({ ...newProduct, description: e.target.value })}
            required
          />
        </Form.Group>

        <Form.Group controlId="productPrice">
          <Form.Label>Price</Form.Label>
          <Form.Control
            type="number"
            step="0.01"
            placeholder="Enter product price"
            value={newProduct.price}
            onChange={(e) => setNewProduct({ ...newProduct, price: e.target.value })}
            required
          />
        </Form.Group>

        <Form.Group controlId="productActive">
         <Form.Label> Availability </Form.Label>
          <Form.Check
            type="radio"
            label="Available"
            checked={newProduct.isActive}
            onChange={(e) => setNewProduct({ ...newProduct, isActive: e.target.checked })}
          />
        </Form.Group>

        <Button className="button" variant="primary" type="submit">
          {editProduct ? 'Update Product' : 'Add Product'}
        </Button>
      </Form>
</div>


<div>

      <h2 className="title">Display Products</h2>
      
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Delete</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th>Activation</th>
            <th>Update</th>
          </tr>
        </thead>
        <tbody>

          {products.map((product) => (
            <tr key={product._id}>
                <td>

                <Button variant="danger" onClick={() => forDeleteProduct(product._id)}>
                  Delete
                </Button>
              </td>

              <td>{product.name}</td>
              <td>{product.description}</td>
              <td>₱ {product.price}</td>
              <td align="center">{product.isActive ? 'Available' : 'Unavailable'}</td>


             
              <td>
                <Button
                  variant={product.isActive ? 'danger' : 'success'}
                  onClick={() => forAchivingActivingProduct(product._id, product.isActive)}
                >
                  {product.isActive ? 'Deactivate' : 'Activate'}


                </Button>

                </td>

                 <td>
                <Button variant="primary" onClick={() => forUpdateProduct(product)}>
                  Update
                </Button>
              </td>

            </tr>
          ))}

        </tbody>
      </Table>

      
    </div>
    </Container>



    )
}

