//import {useState, useEffect, useContext} from 'react';
import {useState, useEffect} from 'react';

//import { Button, Row, Col, Card, Table, Form, Container} from 'react-bootstrap';
import { Button, Table, Container} from 'react-bootstrap';

import {Link} from 'react-router-dom';
//import UserContext from '../UserContext';

//export default function ProductDisplay({products}) {
export default function CustomerProductDisplay() {

//const {name, description, price, isActive, _id} = products;

//const {user, setUser} = useContext(UserContext);

const [products, setProducts] = useState([]);


  function getAllActiveProducts () 
  {
    fetch(`${process.env.REACT_APP_API_URL}/products/activeproduct`, {
     headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}` 
      }
    })
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      })
      .catch((error) => {
        console.error('Error retrieving products:', error);
      });
  };

  useEffect(() => {
    getAllActiveProducts();

  }, [products]);


return (
   
					    <Container>
					    <div>
					     <h2 className="title">Display Products</h2>

					     <Table striped bordered hover>
					       <thead>
					   	       <tr>
					             <th>Buy</th>
					             <th>Name</th>
					             <th>Description</th>
					            	<th>Price</th>
					           </tr>
					         </thead>
					         <tbody>
					           {products.map((product) => (
					             <tr key={product._id}>

					             <td> <Button className="bg-primary" as={Link} to={`/customerproductdisplay/${product._id}`}> Buy </Button> </td>
					               <td>{product.name}</td>
					               <td>{product.description}</td>
					               <td>₱ {product.price}</td>
					            
					            
					             </tr>
					           ))}

					        </tbody>
					       </Table>

					      
					     </div>
					     </Container>

    )
}

