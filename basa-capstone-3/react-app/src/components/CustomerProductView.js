import { useState, useContext, useEffect } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
//import { useParams, Link, useNavigate } from 'react-router-dom';
import { useParams, useNavigate } from 'react-router-dom';


import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function CustomerProductView() {
  

  const {user} = useContext(UserContext);
  const navigate = useNavigate();


  const {productId} = useParams();


  const [name, setName] = useState("Name Here");
  const [description, setDescription] = useState("Description Here");
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);

 function checkoutOrder (productId) 
 {

    fetch(`${process.env.REACT_APP_API_URL}/orders/createorder/${productId}`, {
      method: "PATCH",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        userId: user.id,
        productId: productId,
        name: name, 
        description: description,
        price: price, 
        quantity: quantity

      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if (data) 
      {
        Swal.fire({
          title: `Successfully ordered`,
          icon: "success",
          text: "Order Complete."
        });

        navigate("/customerdashboard")

      } 
      else 
      {
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again."
        });
      }
    });
  };

    
      const qty = () => {
        return qty
      }

      const handleMinus = () => {
        if (quantity > 1 ) {
          setQuantity(qty() - 1);
        }
        else 
        {

        }
      };
      
      const handlePlus = () => {
        if (quantity < 5) {
          setQuantity(qty => qty + 1);
        } else {
          alert("5 items only.");
        }
      };


      const calculateSubTotal = () => {
        return price * quantity;
      };

  useEffect(() => {
    console.log(productId);
    console.log("===");

    fetch(`${process.env.REACT_APP_API_URL}/products/singleproduct/${productId}`)
      .then(res => res.json())
      .then(data => {
        console.log("===");
        console.log(data);
        console.log("===");

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [productId]);

  return (
  (user.id)
          ?       
                (user.isAdmin) 
                ?
                <h1> Unathorized to Administrator</h1>
                :
                <>
                    <Container>
                      <Row>
                        <Col lg={{ span: 6, offset: 3 }}>
                          <Card>
                            <Card.Body className="text-center">
                              <Card.Title>{name}</Card.Title>
                              <Card.Subtitle>Description:</Card.Subtitle>
                              <Card.Text>{description}</Card.Text>
                              <Card.Subtitle>Price:</Card.Subtitle>
                              <Card.Text>PhP {price}</Card.Text>
                                  <div>
                                    <Button variant="outline-success" onClick={handleMinus}>
                                      -
                                    </Button>
                                    
                                    <span>{quantity}</span>
                                    
                                    <Button variant="outline-success" onClick={handlePlus}>
                                      +
                                    </Button>
                                  </div>

                                  <div> Total Amount: PhP {calculateSubTotal()}.00</div>

                                  <Button variant="success" onClick={() => checkoutOrder(productId)}>
                                    Checkout
                                  </Button>
                            </Card.Body>
                          </Card>
                        </Col>
                      </Row>
                    </Container>
                </>
            :
            <h1> Unathorized to Guest </h1>
  );
}
