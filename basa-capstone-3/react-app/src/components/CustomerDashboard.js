import Banner from './Banner';
import CustomerProductDisplay from './CustomerProductDisplay';


//import React, {useState, useEffect, useContext} from 'react'
import React, {useContext} from 'react'

import UserContext from '../UserContext';



export default function CustomerDashboard() {

//const {user, setUser} = useContext(UserContext);
const {user} = useContext(UserContext);

	const data = {
		title: "Welcome to Play-tendo, customer!",
		content: "Your one-stop shop for your gaming experience. ",
		destination: "/customerdashboard",
		label: "Shop Now"
	}

	//const {user, setUser} = useContext(UserContext);


	return (

	(user.id)
		?       
		    (user.isAdmin) 
		    ?
		   	<h1> Unathorized </h1>
		   	:
			<>
			<Banner data={data} />
			<hr/>
			<CustomerProductDisplay />
			</>
		:
		<h1> Unathorized </h1>
	)
}


