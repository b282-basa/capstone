

import {Nav, Navbar} from 'react-bootstrap';

import {Link, NavLink} from 'react-router-dom'

import {useContext} from 'react'

import UserContext from '../UserContext';

export default function AppNavbar() {

  const {user}  = useContext(UserContext);

  return (
    <Navbar expand="lg" className="bg-body-tertiary">

        <Navbar.Brand as={Link} to="/">Play-tendo</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
           
            
            { 
              (user.id) 
              ?
                    (user.isAdmin) ?
                      <>
                        {/*<Nav.Link as={NavLink} to="/admindashboard"> Home </Nav.Link>*/}
                        <Nav.Link as={NavLink} to="/admindashboard"> Admin Dashboard </Nav.Link>
                        <Nav.Link as={NavLink} to="/logout"> Logout </Nav.Link>
                      </>
                    :
                    <>
                        <Nav.Link as={NavLink} to="/customerdashboard"> Customer Dashboard </Nav.Link>
                        {/*<Nav.Link as={NavLink} to="/customerproductorderhistory"> Order History </Nav.Link>*/}
                        <Nav.Link as={NavLink} to="/logout"> Logout </Nav.Link>
                      </>
              :
                
              <>
                <Nav.Link as={NavLink} to="/">Home</Nav.Link>    
                <Nav.Link as={NavLink} to="/register"> Register</Nav.Link>
                <Nav.Link as={NavLink} to="/login"> Login </Nav.Link>
              </>
            }

          </Nav>
        </Navbar.Collapse>

    </Navbar>
  );
}
