import Banner from '../components/Banner';


export default function Home() {

	const data = {
		title: "Play-tendo",
		content: "Your one-stop shop for your gaming experience",
		destination: "/login",
		label: "Login now!"
	}


	return (
		<>
		<Banner data={data} />
		</>
	)
}


