import {useState, useEffect, useContext} from 'react';

import { Button, Row, Col, Card } from 'react-bootstrap';

import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function AdminProductDisplay({product, editHandler, deleteHandler}) {

const {name, description, price, isActive, _id} = product;

const {user, setUser} = useContext(UserContext);



return (


    // <Row className="mt-3 mb-3">
    //     <Col xs={12}>
    //         <Card className="cardHighlight p-0">
    //             <Card.Body>
    //                  <Card.Img variant="top" src="holder.js/100px180" />
    //                 <Card.Title><h4>{name}</h4></Card.Title>
    //                 <Card.Text>{description}</Card.Text>
    //                 <Card.Text>{price}</Card.Text>
    //                 <Button className="bg-primary" as={Link} to={`/activeproduct/${_id}`}>Details</Button>
    //             </Card.Body>
    //         </Card>
    //     </Col>
    // </Row>      
    
   
   
   <Row className="mt-3 mb-3">
         <Col xs={12}>
             <Card className="cardHighlight p-0">
                 <Card.Body>
                      <Card.Img variant="top" src="holder.js/100px180" />
                     <Card.Title><h4>{name}</h4></Card.Title>
                     <Card.Text>{description}</Card.Text>
                     <Card.Text> Php {price}.00</Card.Text>

                    <>
                        {
                            user.isAdmin ?
                            <>

                                {/*<Button className="bg-primary" as={Link} to={`allproduct/${_id}`}>Checkout Details</Button>*/}
                                <Button className="bg-primary" name={_id} as={Link} onClick={editHandler}> Edit </Button>
                            </>
                            :
                            <>
                                <Button className="bg-primary" as={Link} to={`activeproduct/${_id}`}>Details</Button>
                                <Button className="bg-primary" as={Link} to={`activeproduct/${_id}`}>Details</Button>
                            </>
                        }   </>
                 </Card.Body>
             </Card>
         </Col>
     </Row>      
    )
}

