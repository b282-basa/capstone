
import ProductDisplay from '../components/ProductDisplay';

import AdminCreateProduct from '../components/AdminCreateProduct';

import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext';

export default function AdminProductManagement () {
	
	const {user, setUser} = useContext(UserContext);

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/allproduct`)
		.then(res => res.json())
		.then(data => {
			setProducts(data.map(product =>{
				return(
					<ProductDisplay key={product.id} products={product} />
					)
			}))
		})}, []);

	return (
		user.isAdmin ?

		<>
		<h1> Admin Products Management</h1>
		{products}

		</>
		 :
		 <h1> Unathorized</h1>

	)
};
