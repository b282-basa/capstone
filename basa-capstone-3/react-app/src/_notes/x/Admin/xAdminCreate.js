import Button from "react-bootstrap/button";
import React, { useState } from "react";

import { createProduct } from "./api";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";

export default function AdminCreate() {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);

  const handleSubmit = () => {
    const product = {
      name: name,
      description: description,
      price: price,
    };

    createProduct(product).then((response) => {
      console.log("done", response);
    });
  };
  return (
    <>
      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "25ch" },
        }}
        noValidate
        autoComplete="off"
      >
        {/*<TextField size="lg" id="outlined-basic" label="Cin" onChange={e => setStudentId(e.target.value)}/>*/}
        <TextField id="outlined-basic" label="Name"  onChange={e => setName(e.target.value)}/>
        <TextField id="outlined-basic" label="Description"  onChange={e => setDescription(e.target.value)}/>
        <TextField id="outlined-basic" label="Price" onChange={e => setPrice(e.target.value)}/>
       
          <Button variant="primary" type="button" onClick={handleSubmit}>
            Add product now
          </Button>
      
      </Box>
    </>
    
  );
}