import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import React, { useEffect,useState } from "react";
import { updateProduct } from "./api";


import TextField from "@mui/material/TextField";
import { getOneProduct } from "./api";


export default function AdminUpdate(props) {

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
 /* const [studentId, setStudentId] = useState("");*/
  const [oneProduct,setOneProduct] = useState("");

  useEffect(() => {
    getOneProduct(props.id).then((response) => {
      setOneProduct(response.data)
      console.log("data====>",response.data)
    })
  }, []); 

  const handleSubmit = () => {
    
    const product = {
      name: name,
      description: description,
      price: price,
    };

    updateProduct(props.id, product).then((response) => {
      console.log("done", response);
    });
  };

  return (
    <>
   
   <>
      <Button variant="primary" onClick={handleShow}>
        Update
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update product </Modal.Title>
        </Modal.Header>
        <Modal.Body>    
          <TextField id="outlined-basic" fullWidth placeholder={oneProduct.name}   onChange={e => setName(e.target.value)}/> <br></br><br></br>
          <TextField id="outlined-basic" fullWidth placeholder={oneProduct.description}  onChange={e => setDescription(e.target.value)}/><br></br><br></br>
          <TextField id="outlined-basic" fullWidth placeholder={oneProduct.price} onChange={e => setPrice(e.target.value)}/><br></br><br></br>
          {/* <Button variant="primary" type="button">
            Update
          </Button> */}
          </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary"  onClick={handleSubmit}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
    
    
      
    
    </>
  );
}