import axios from 'axios'

const BASE_URL = `process.env.REACT_APP_API_URL`

export  const fetchProducts =  async () => {
         return axios.get(`${BASE_URL}/products/allproduct`)  
/*return 
{
fetch(`${process.env.REACT_APP_API_URL}/products/allproduct`)
    .then(res => res.json())
    .then(data => {
      setProducts(data.map(product =>{
        return(
          <AdminProduct key={product.id} products={product}  />
          )
      }))
    })}*/


}

export const getOneProduct = async (id) => {
  return await axios.get(`${BASE_URL}/products/singleproduct/${id}`)
}

export const createProduct = async (newProduct) => {
   console.log(newProduct)
   await axios.post(`${BASE_URL}/products/createproduct`,newProduct).then((response) =>{
      console.log(response.data)
      return window.location.reload(false);
   

   })
}

export const deleteProduct  = async (id) => {
   await axios.delete(`${BASE_URL}/products/deleteproduct/${id}`).then((response) =>{
      console.log(response.data)
      return window.location.reload(false);
   })
}


export const updateProduct  = async (id,product) => {
   await axios.put(`${BASE_URL}/products/update/${id}`,product).then((response) =>{
      console.log(response.data)
      return window.location.reload(false);
   })
}