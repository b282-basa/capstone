import { useEffect, useState } from "react";

import { fetchProducts, deleteProduct } from "./api";
import React from "react";
import Table from "react-bootstrap/table";
import Button from "react-bootstrap/button";


import AdminUpdate from "./AdminUpdate";

const baseURL = 'process.env.REACT_APP_API_URL';

export default function AdminProduct(prop) {
  const [products, setProducts] = useState([]);

  const handleDelete = (id) => {
    deleteProduct(id);
  };

  useEffect(() => {
    fetchProducts().then((response) => {
      setProducts(response.data);
    });

  }, []);

  if (!products) return <div> empty </div>;
  return (
    <Table hover >
      <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Price</th>
          <th>Update</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {products.map((product) => (
          <tr>
            <td>{product.name}</td>
            <td>{product.description}</td>
            <td>{product.price}</td>
            <td>
              <AdminUpdate id={product._id}>  </AdminUpdate>
            </td>
            <td>
              <Button
                className="btn btn-danger"
                onClick={(e) => handleDelete(product._id)}
              >
                Delete{" "}
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
}