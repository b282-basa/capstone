import { useState } from "react";
import axios from "axios";

export default function AdminProductUpdate({ _id, closeHandler, updateHandler }) {
  const [products, setProducts] = useState({ title: "", description: "" , price: ""});

  const handleChange = (e) => {
    setProducts((data) => ({ ...data, [e.target.name]: e.target.value }));
  };

  const submitHandler = (e) => {
    e.preventDefault();


    fetch(`${process.env.REACT_APP_API_URL}/products/allproduct`)
    .then(res => res.json())
    .then(data => {
      setProducts(data.map(product =>{
        return(
          <AdminProductDisplay key={product.id} products={product} 

          editHandler={editHandler}
          deleteHandler={deleteHandler} />
          )
      }))
    })}, [update]);
  };

  return (
    <form
      className="form-container"
      onSubmit={(e) => {
        submitHanlder(e);
        updateHandler();
        closeHandler();
      }}
    >
      <label htmlFor="title" className="label">
        Todo Title
      </label>
      <input
        type="text"
        name="title"
        className="input"
        onChange={handleChange}
      />
      <label htmlFor="description" className="label">
        Todo Description
      </label>
      <input
        type="textarea"
        name="description"
        className="input"
        onChange={handleChange}
      />
      <button type="submit" className="todo-btn">
        ➕ Add
      </button>
    </form>
  );
}
