import React, { useState } from 'react';
import { getProductsAPI } from '../api/products'
import CreateProduct from './CreateProduct';

export default function Admin() {
  const [products, setProducts] = useState([])

  const addProduct = (products) => {
    getProductsAPI(products).then(data => {
      setProducts([...products, data])
    })
  }
    return (
      <main role="main" className="container">
        <CreateProduct onCreate={addProduct} />
      </main>  
    )
}
