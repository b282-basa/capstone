import React from 'react'

import {getProductsAPI} from '../api/products'


export default function DisplayProducts(products) {
const { products } = products

return (
<div className="App mt-5">
   <table className="table table-striped">
      <thead>
         <tr>
            <th scope="col">Title</th>
            <th scope="col">Description</th>
            <th scope="col">Price</th>

            <th scope="col">Active</th>
         </tr>
      </thead>
      <tbody>
         {
         products.map(product => {
         return (
         <tr key={product._id}>
            <td>{product.title}</td>
            <td>{product.description}</td>
            <td> {product.price}
              {/* {
               (todo.done) ? 
               (<img width="25" src="../../check.png" />) 
               :
               (<img width="25" src="../../uncheck.jpeg" />)
               }*/}
            </td>
            <td>
               <button className='btn btn-danger'>Delete</button>
            </td>
         </tr>
         )
         })
         }
      </tbody>
   </table>
</div>
)
}
