import React, { useState, Fragment } from 'react'

import {getProductsAPI} from '../api/products'

export default function CreateProduct(product) {
    const { onCreate } = product
    
    /*const {name, description, price} = product;*/

    const [products, setProducts] = useState({
        name: "",
        description: "",
        price: 0
    })

    const onChange = (e) => {
        setProducts({
            ...products,
            [e.target.name]: e.target.value
        })
    }
    
    const saveProduct = (e) => {
        e.preventDefault()
        onCreate(products)
        

    }
    return (
        <Fragment>
                <form onSubmit={saveProduct}>
                    <h2 className="text-center m-3">Products APP</h2>
                    <div className="form-row d-flex justify-content-center">
                        <div className="col-3 m-1">
                                <input 
                                    name = "title" 
                                    type="text" 
                                    className="form-control" 
                                    placeholder="Title"
                                    onChange={(e) => onChange(e)} />
                        </div>
                        
                        <div className="col-5 d-flex justify-content-center m-1">
                                <input 
                                    type="text" 
                                    className="form-control" 
                                    name = "description" 
                                    placeholder="Description"
                                    onChange={(e) => onChange(e)}/>
                        </div>

                        <div className="col-5 d-flex justify-content-center m-1">
                                <input 
                                    type="text" 
                                    className="form-control" 
                                    name = "price" 
                                    placeholder="price"
                                    onChange={(e) => onChange(e)}/>
                        </div>

                        <button className='btn btn-primary col-2 d-flex justify-content-center m-1' 
                            type='submit'>Add Product</button>
                    </div>
                </form>
        </Fragment>
    )
}
