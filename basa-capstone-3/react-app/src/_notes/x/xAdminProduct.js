import { useState, useEffect, useContext} from "react";
import axios from "axios";
import { Link } from "react-router-dom";



import AdminProductDisplay from "./AdminProductDisplay";
//import AdminProductUpdate from "./AdminProductUpdate";

export default function AdminProduct() {
  const [id, setId] = useState("");
  const [update, setUpdate] = useState(false);

  const [products, setProducts] = useState([]);
  const [modal, setModal] = useState(false);

  useEffect(() => {

    fetch(`${process.env.REACT_APP_API_URL}/products/allproduct`)
    .then(res => res.json())
    .then(data => {
      setProducts(data.map(product =>{
        return(
          <AdminProductDisplay key={product.id} products={product} 

          editHandler={editHandler}
          deleteHandler={deleteHandler} />
          )
      }))
    })}, [update]);


  const editHandler = (e) => {
    setId(e.target.name);
    setModal(true);
  };

  const updateHandler = () => {
    setUpdate(!update);
  };

  const deleteHandler = (e) => {
    axios.delete(`${process.env.REACT_APP_API_URL}/products/allproduct/${e.target.name}`);

    setProducts((data) => {
      return data.filter((product) => product._id !== e.target.name);
    });
  };

  const closeHandler = () => {
    setId("");
    setModal(false);
  };

  return (

    //CREATE
    <section className="container">
      <Link to="/admincreateproduct" className="button-createproduct">
        <button className="createproduct-btn">➕ Add new product</button>
      </Link>

      {products}

      </section>

    /* {modal 
      ? 
      (
        <section className="update-container">
          <div className="update-todo-data">
            <p onClick={closeHandler} className="close">
              &times; 
            </p>

            <AdminProductUpdate
              _id={id}
              closeHandler={closeHandler}
              updateHandler={updateHandler}
            />
          </div>
        </section>
      ) 
      : 
      (
        ""
      )*/

  );
}

