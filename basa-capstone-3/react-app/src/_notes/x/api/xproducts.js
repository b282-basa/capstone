const API_URL = 'process.env.REACT_APP_API_URL';

// ALL PRODUCT
export async function getProductsAPI(){
    return fetch(`${API_URL}/products/allproduct`)
    .then(resp => resp.json())
    .then(data => data)
    .catch(e => console.log(e))
}

// CREATE PRODUCT
export async function createProductsAPI(product){
    return fetch(`${API_URL}/products/createproduct`,{
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(product)
    })
    .then(resp => {
        return resp.json()
    })
    .then(data => data)
    .catch(e => console.log(e))
}

// UPDATE ONLY THE ACTIVE STATUS
export async function archiveProductAPI(id, isActive){
    let product = {
        _id: id,
        isActive: isActive
    }
    return fetch(`${API_URL}/products/archiveProduct/${id}`,{
        method: "PATCH",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(product)
    })
    .then(resp => resp.json())
    .then(data => data)
    .catch(e => console.log(e))
}


export async function deleteProductAPI(id){
    return fetch(`${API_URL}/products/deleteproduct/${id}`,{
        method: "DELETE"
    })
    .then(resp => resp.json())
    .then(data => data)
    .catch(e => console.log(e))
}