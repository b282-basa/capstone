/*import {useState, useEffect, useContext} from 'react';

import { Button, Row, Col, Card, Table, Form, Container} from 'react-bootstrap';

import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

//export default function ProductDisplay({products}) {
export default function CustomerProductOrderHistory() {

//const {name, description, price, isActive, _id} = products;

const {user, setUser} = useContext(UserContext);

const [orders, setOrders] = useState([]);


  function getAllUserOrder (userId) 
  {
    fetch(`${process.env.REACT_APP_API_URL}/orders/userorder/${userId}`, {
     headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}` 
      }
    })
      .then((res) => res.json())
      .then((data) => {
        setOrders(data);
      })
      .catch((error) => {
        console.error('Error fetching products:', error);
      });
  };

  useEffect(() => {
    getAllUserOrder();

    // retrieve user orders
  }, [orders]);


return (
   
					    <Container>
					    <div>
					     <h2 className="title">Display Orders</h2>

					     {/* Table to display list of products */}
					     <Table striped bordered hover>
					       <thead>
					   	       <tr>
					             <th>Buy</th>
					             <th>Name</th>
					             <th>Description</th>
					            	<th>Price</th>
					           {/*<th>Availability</th>
					             <th colspan="3">Actions</th>*/}
					           </tr>
					         </thead>
					         <tbody>
					           {orders.map((order) => (
					             <tr key={order._id}>
					               <td>{order.productId}</td>
					               <td>{order.productName}</td>
					               <td>₱{order.price}</td>
					               <td>{order.quantity}</td>
					               <td>₱{order.subtotal}</td>
					            
					             </tr>
					           ))}

					        </tbody>
					       </Table>

					      
					     </div>
					     </Container>

    )
}

*/