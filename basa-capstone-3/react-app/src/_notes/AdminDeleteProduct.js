import React, { Component, useEffect, useState } from "react";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


export default function AdminDeleteProduct() {

  //setting state
  const [data, setData] = useState([]);


  useEffect(() => {

    getAllProducts();

  }, []);


  //fetching all user
  const getAllProducts = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/allproduct`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data, "productData");
        //setData(data)
      });
  };


  //deleting user
  /*const deleteProduct = (id, name) => {
    if (window.confirm(`Are you sure you want to delete ${name}`)) {
      fetch(`${process.env.REACT_APP_API_URL}/products/deleteproduct`, {
        method: "POST",
        crossDomain: true,
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({
          _id: id,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          alert(data.data);
          getAllProducts();
        });
    }
    }*/

  return ( 
    <div className="auth-wrapper" style={{ height: "auto" }}>
      <div className="auth-inner" style={{ width: "auto" }}>
        <h3>Delete Products</h3>
        <table style={{ width: 500 }}>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Active</th>
            <th>Delete</th>
          </tr>
          {setData(data.map(product => {
            return (
              <tr>
                <td>{product.name}</td>
                <td>{product.description}</td>
                <td>{product.price}</td>
                <td>{product.active}</td>
                <td>
                 {/* <FontAwesomeIcon
                    icon={faTrash}
                    onClick={() => deleteProduct(product._id, product.name)}
                  />*/}
                </td>
              </tr>
            );
          }))}

          </table>


            </div>
              </div>
            );
          }
