import AppNavbar from './components/AppNavbar';


import Home from './pages/Home';
//import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';



import CustomerDashboard from './components/CustomerDashboard';
import CustomerProductDisplay from './components/CustomerProductDisplay';
import CustomerProductView from './components/CustomerProductView';


import AdminDashboard from './components/AdminDashboard';
import AdminProductDisplay from './components/AdminProductDisplay';


import {BrowserRouter as Router, Routes, Route}  from 'react-router-dom'

import './App.css';

import {Container} from 'react-bootstrap';

import {UserProvider} from './UserContext';

import {useState, useEffect} from 'react';

export default function App() {

  const [user, setUser] = useState({
      id: null,
      isAdmin: null
    });

   useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then( data => {

      // user us logged in
      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
        // user is logged out
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, []);

    const unsetUser = () => 
    {
      localStorage.clear();
    }

    return (
    // 
    <>
      <UserProvider value={{user, setUser, unsetUser}}> 
        <Router>
          <AppNavbar />
          <Container >
            <Routes>
              <Route path="/" element={<Home />} />

             {/*<Route path="/courses" element={<Courses />} />

              <Route path="/courses/:courseId" element={<CourseView />} />*/}

              <Route path="/*" element={<Error/>} />

              <Route path="/register" element={<Register />} /> 

              <Route path="/login" element={<Login />} />

              <Route path="/logout" element={<Logout />} />

              
              
              <Route path="/customerdashboard" element={<CustomerDashboard/>} />
              <Route path="/customerproductdisplay" element={<CustomerProductDisplay/>} />
              <Route path="/customerproductdisplay/:productId" element={<CustomerProductView/>} />
           

              <Route path="/admindashboard" element={<AdminDashboard/>} />
              <Route path="/adminproductdisplay" element={<AdminProductDisplay/>} />

            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}
